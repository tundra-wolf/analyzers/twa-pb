# PowerBuilder Analyzer - Specs

## Versions

### All current versions

All currently supported versions of PowerBuilder have their documentation on the [Docs website](https://docs.appeon.com/).

### 2022 R3

The What's New documentation for this version of PowerBuilder is [here](https://docs.appeon.com/pb/whats_new/pb2022r3.html).

### 2019 R3

The What's New documentation for this version of PowerBuilder is [here](https://docs.appeon.com/pb/whats_new/pb2022r3.html).

### Older versions

Older versions of PowerBuilder are documented on the [Sybase Document Archives](https://infocenter-archive.sybase.com/help/index.jsp?topic=/com.sybase.dc30032_1150/html/pbinst/BABBEBAF.htm).
